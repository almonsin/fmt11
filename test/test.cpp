/**
* Copyright (c) 2015 Peter Belanyi (peter.belanyi@gmail.com)
*
* Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
* or copy at http://opensource.org/licenses/MIT)
*/

#include "catch.hpp"

#include <sstream>
#include <fmt11.h>


struct CustomType {
    const char * str;
    CustomType(const char * str)
        : str(str) {}
};
inline const std::string to_string(const CustomType& ct) { return std::string(ct.str); }

struct CustomTypeW {
    const wchar_t * str;
    CustomTypeW(const wchar_t * str)
        : str(str) {}
};
inline const std::wstring to_wstring(const CustomTypeW& ct) { return std::wstring(ct.str); }

TEST_CASE( "Append", "[append]" ) {
    SECTION("constructor + conversion to string") {
        REQUIRE( std::string("") == fmt11::Append().str() );
        REQUIRE( std::string("hello") == fmt11::Append("hello").str() );
        REQUIRE( std::string("hello") == std::string(fmt11::Append("hello")) );
    }

    SECTION("to_string for different types") {
        REQUIRE( std::string("1234") == fmt11::Append(1234).str() ); // int
        REQUIRE( std::string("1234") == fmt11::Append(1234L).str() ); // long
        REQUIRE( std::string("1234") == fmt11::Append(1234LL).str() ); // long long
        REQUIRE( std::string("1234") == fmt11::Append(1234U).str() ); // unsigned
        REQUIRE( std::string("1234") == fmt11::Append(1234UL).str() ); // unsigned long
        REQUIRE( std::string("1234") == fmt11::Append(1234ULL).str() ); // unsigned long long
        REQUIRE( std::string("3.141593") == fmt11::Append(3.14159265f).str() ); // float
        REQUIRE( std::string("3.141593") == fmt11::Append(3.14159265).str() ); // double
        REQUIRE( std::string("3.141593") == fmt11::Append(3.14159265L).str() ); // long double
        REQUIRE( std::string("x") == fmt11::Append('x').str() ); // char
        REQUIRE( std::string("hello") == fmt11::Append("hello").str() ); // const char *
        REQUIRE( std::string("hello") == fmt11::Append(std::string("hello")).str() ); // std::string
        REQUIRE( std::string("hello") == fmt11::Append(CustomType("hello")).str() ); // custom type
    }

    auto appender = fmt11::Append("hello ");

    SECTION("appending") {
        appender + "world";
        REQUIRE( std::string("hello world") == appender.str() );
        appender + ' ';
        REQUIRE( std::string("hello world ") == appender.str() );
        appender + 42;
        REQUIRE( std::string("hello world 42") == appender.str() );
        appender + ' ' + 3.14159265;
        REQUIRE( std::string("hello world 42 3.141593") == appender.str() );
        appender + std::string("!");
        REQUIRE( std::string("hello world 42 3.141593!") == appender.str() );
    }

    SECTION("appending with operator <<") {
        REQUIRE(std::string("hello world 42 3.141593") == (appender << "world " << 42 << ' ' << 3.14159265).str());
    }

    SECTION("custom type") {
        appender + CustomType("world");
        REQUIRE( std::string("hello world") == appender.str() );
    }

    SECTION("writing into stream") {
        std::stringstream ss;
        ss << fmt11::Append("hello ") + "world " + 42 + ' ' + 3.14159265;
        REQUIRE( std::string("hello world 42 3.141593") == ss.str() );
    }
}


TEST_CASE( "Append Wide", "[append]" ) {
    SECTION("constructor + conversion to string") {
        REQUIRE( std::wstring(L"") == fmt11::AppendW().str() );
        REQUIRE( std::wstring(L"hello") == fmt11::AppendW(L"hello").str() );
        REQUIRE( std::wstring(L"hello") == std::wstring(fmt11::AppendW(L"hello")) );
    }

    SECTION("to_wstring for different types") {
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234).str() ); // int
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234L).str() ); // long
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234LL).str() ); // long long
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234U).str() ); // unsigned
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234UL).str() ); // unsigned long
        REQUIRE( std::wstring(L"1234") == fmt11::AppendW(1234ULL).str() ); // unsigned long long
        REQUIRE( std::wstring(L"3.141593") == fmt11::AppendW(3.14159265f).str() ); // float
        REQUIRE( std::wstring(L"3.141593") == fmt11::AppendW(3.14159265).str() ); // double
        REQUIRE( std::wstring(L"3.141593") == fmt11::AppendW(3.14159265L).str() ); // long double
        REQUIRE( std::wstring(L"x") == fmt11::AppendW(L'x').str() ); // wchar_t
        REQUIRE( std::wstring(L"hello") == fmt11::AppendW(L"hello").str() ); // const wchar_t *
        REQUIRE( std::wstring(L"hello") == fmt11::AppendW(std::wstring(L"hello")).str() ); // std::wstring
        REQUIRE( std::wstring(L"hello") == fmt11::AppendW(CustomTypeW(L"hello")).str() ); // custom type
    }

    auto appender = fmt11::AppendW(L"hello ");

    SECTION("appending") {
        appender + L"world";
        REQUIRE( std::wstring(L"hello world") == appender.str() );
        appender + L' ';
        REQUIRE( std::wstring(L"hello world ") == appender.str() );
        appender + 42;
        REQUIRE( std::wstring(L"hello world 42") == appender.str() );
        appender + L' ' + 3.14159265;
        REQUIRE( std::wstring(L"hello world 42 3.141593") == appender.str() );
        appender + std::wstring(L"!");
        REQUIRE( std::wstring(L"hello world 42 3.141593!") == appender.str() );
    }

    SECTION("appending with operator <<") {
        REQUIRE(std::wstring(L"hello world 42 3.141593") == (appender << L"world " << 42 << L' ' << 3.14159265).str());
    }

    SECTION("custom type") {
        appender + CustomTypeW(L"world");
        REQUIRE( std::wstring(L"hello world") == appender.str() );
    }

    SECTION("writing into stream") {
        std::wstringstream ss;
        ss << fmt11::AppendW(L"hello ") + L"world " + 42 + L' ' + 3.14159265;
        REQUIRE( std::wstring(L"hello world 42 3.141593") == ss.str() );
    }
}


TEST_CASE( "Replace", "[replace]" ) {
    SECTION("constructor + conversion to string") {
        REQUIRE( std::string("") == fmt11::Replace("").str() );
        REQUIRE( std::string("hello") == fmt11::Replace("hello").str() );
        REQUIRE( std::string("hello") == fmt11::Replace("hello", "{}").str() );
        REQUIRE( std::string("hello") == std::string(fmt11::Replace("hello")) );
    }

    SECTION("to_string for different types") {
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234)).str() ); // int
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234L)).str() ); // long
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234LL)).str() ); // long long
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234U)).str() ); // unsigned
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234UL)).str() ); // unsigned long
        REQUIRE( std::string("1234") == (fmt11::Replace("%s") % (1234ULL)).str() ); // unsigned long long
        REQUIRE( std::string("3.141593") == (fmt11::Replace("%s") % (3.14159265f)).str() ); // float
        REQUIRE( std::string("3.141593") == (fmt11::Replace("%s") % (3.14159265)).str() ); // double
        REQUIRE( std::string("3.141593") == (fmt11::Replace("%s") % (3.14159265L)).str() ); // long double
        REQUIRE( std::string("x") == (fmt11::Replace("%s") % ('x')).str() ); // char
        REQUIRE( std::string("hello") == (fmt11::Replace("%s") % ("hello")).str() ); // const char *
        REQUIRE( std::string("hello") == (fmt11::Replace("%s") % (std::string("hello"))).str() ); // std::string
        REQUIRE( std::string("hello") == (fmt11::Replace("%s") % (CustomType("hello"))).str() ); // custom type
    }

    auto replacer = fmt11::Replace("hello %s");

    SECTION("replacing with const char *") {
        replacer % "world";
        REQUIRE( std::string("hello world") == replacer.str() );
    }
    SECTION("replacing with int") {
        replacer % 42;
        REQUIRE( std::string("hello 42") == replacer.str() );
    }
    SECTION("replacing with double") {
        replacer % 3.14159265;
        REQUIRE( std::string("hello 3.141593") == replacer.str() );
    }
    SECTION("replacing with char") {
        replacer % '!';
        REQUIRE( std::string("hello !") == replacer.str() );
    }
    SECTION("replacing with string") {
        replacer % std::string("world");
        REQUIRE( std::string("hello world") == replacer.str() );
    }
    SECTION("replacing with custom type") {
        replacer % CustomType("world");
        REQUIRE( std::string("hello world") == replacer.str() );
    }

    SECTION("replacing with custom placeholder") {
        auto replacer = fmt11::Replace("hello {} {}{}{}", "{}") % "world" % 42 % ' ' % 3.14159265;
        REQUIRE( std::string("hello world 42 3.141593") == replacer.str() );
    }

    SECTION("replacing with operator <<") {
        REQUIRE(std::string("hello world 42 3.141593") == (fmt11::Replace("hello {} {}{}{}", "{}") << "world" << 42 << ' ' << 3.14159265).str());
    }

    SECTION("writing into stream") {
        std::stringstream ss;
        ss << fmt11::Replace("hello %s %s%s%s") % "world" % 42 % ' ' % 3.14159265;
        REQUIRE( std::string("hello world 42 3.141593") == ss.str() );
    }

    SECTION("excetions") {
        REQUIRE_THROWS_AS( replacer % "world" % 42, fmt11::Exception );
    }
}


TEST_CASE( "Replace Wide", "[replace]" ) {
    SECTION("constructor + conversion to string") {
        REQUIRE( std::wstring(L"") == fmt11::ReplaceW(L"").str() );
        REQUIRE( std::wstring(L"hello") == fmt11::ReplaceW(L"hello").str() );
        REQUIRE( std::wstring(L"hello") == fmt11::ReplaceW(L"hello", L"{}").str() );
        REQUIRE( std::wstring(L"hello") == std::wstring(fmt11::ReplaceW(L"hello")) );
    }

    SECTION("to_string for different types") {
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234)).str() ); // int
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234L)).str() ); // long
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234LL)).str() ); // long long
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234U)).str() ); // unsigned
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234UL)).str() ); // unsigned long
        REQUIRE( std::wstring(L"1234") == (fmt11::ReplaceW(L"%s") % (1234ULL)).str() ); // unsigned long long
        REQUIRE( std::wstring(L"3.141593") == (fmt11::ReplaceW(L"%s") % (3.14159265f)).str() ); // float
        REQUIRE( std::wstring(L"3.141593") == (fmt11::ReplaceW(L"%s") % (3.14159265)).str() ); // double
        REQUIRE( std::wstring(L"3.141593") == (fmt11::ReplaceW(L"%s") % (3.14159265L)).str() ); // long double
        REQUIRE( std::wstring(L"x") == (fmt11::ReplaceW(L"%s") % (L'x')).str() ); // wchar_t
        REQUIRE( std::wstring(L"hello") == (fmt11::ReplaceW(L"%s") % (L"hello")).str() ); // const wchar_t *
        REQUIRE( std::wstring(L"hello") == (fmt11::ReplaceW(L"%s") % (std::wstring(L"hello"))).str() ); // std::wstring
        REQUIRE( std::wstring(L"hello") == (fmt11::ReplaceW(L"%s") % (CustomTypeW(L"hello"))).str() ); // custom type
    }

    auto replacer = fmt11::ReplaceW(L"hello %s");

    SECTION("replacing with const char *") {
        replacer % L"world";
        REQUIRE( std::wstring(L"hello world") == replacer.str() );
    }
    SECTION("replacing with int") {
        replacer % 42;
        REQUIRE( std::wstring(L"hello 42") == replacer.str() );
    }
    SECTION("replacing with double") {
        replacer % 3.14159265;
        REQUIRE( std::wstring(L"hello 3.141593") == replacer.str() );
    }
    SECTION("replacing with char") {
        replacer % L'!';
        REQUIRE( std::wstring(L"hello !") == replacer.str() );
    }
    SECTION("replacing with wstring") {
        replacer % std::wstring(L"world");
        REQUIRE( std::wstring(L"hello world") == replacer.str() );
    }
    SECTION("replacing with custom type") {
        replacer % CustomTypeW(L"world");
        REQUIRE( std::wstring(L"hello world") == replacer.str() );
    }

    SECTION("replacing with custom placeholder") {
        auto replacer = fmt11::ReplaceW(L"hello {} {}{}{}", L"{}") % L"world" % 42 % L' ' % 3.14159265;
        REQUIRE( std::wstring(L"hello world 42 3.141593") == replacer.str() );
    }

    SECTION("replacing with operator <<") {
        REQUIRE(std::wstring(L"hello world 42 3.141593") == (fmt11::ReplaceW(L"hello {} {}{}{}", L"{}") << L"world" << 42 << L' ' << 3.14159265).str());
    }

    SECTION("writing into stream") {
        std::wstringstream ss;
        ss << fmt11::ReplaceW(L"hello %s %s%s%s") % L"world" % 42 % L' ' % 3.14159265;
        REQUIRE( std::wstring(L"hello world 42 3.141593") == ss.str() );
    }

    SECTION("excetions") {
        REQUIRE_THROWS_AS( replacer % L"world" % 42, fmt11::Exception );
    }
}
