/**
* Copyright (c) 2015 Peter Belanyi (peter.belanyi@gmail.com)
*
* Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
* or copy at http://opensource.org/licenses/MIT)
*/

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
