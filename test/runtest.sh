#!/bin/bash

uname --all
mkdir build
cd build
cmake ..
make
ctest --output-on-failure
