fmt11
---------

fmt11 is a simple and easy to use string formatting library for C++11

See fmt11 website https://gitlab.com/almonsin/fmt11 on GitLab.

Keywords: string, format, C++11

### About fmt11:

fmt11 is an open source **header-only** library that implements **string formatting** using **C++11** features.
It is **type safe**, and can be extended to work with **custom types**. It does not use any macro-magic, instead relies on C++ templates and overloading.
An appender and a replacer class is provided for convenient usage. Besides ASCII strings it supports **wide character strings** too.
It relies on std::to_string for conversion to string, which allows for a simple implementation and extensibility,
but imposes some limitations, e.g. there is no special formatting for types.

## Tutorial

### The first sample demonstrates how to use the formatter class: 

```C++
#include <iostream>
#include <fmt11.h>

using namespace std;
int main()
{
    try {
        // fmt11::Replace takes a format string and replaces the placeholders with the provided values (of different types)
        cout<< fmt11::Replace("%s %s, %s %s!") % "hello" % "world" % 42 % 3.14 <<endl;

        // operator << can be used too, but it has different precedence
        cout<< (fmt11::Replace("%s %s, %s %s!") << "hello" << "world" << 42 << 3.14) <<endl;

        // it also supports wide strings
        wcout<< fmt11::ReplaceW(L"%s %s, %s %s!") % L"hello" % L"world" % 42 % 3.14 <<endl;

        // the placeholder can be changed passing a second parameter to Replace
        cout<< fmt11::Replace("{} {}, {} {}!", "{}") % "hello" % "world" % 42 % 3.14 <<endl;

        // besides piping into streams, Append and Replace can be converted to std::string implicitly, or explicitly by calling the .str() method
        string greetings = fmt11::Replace("{} {}", "{}") % "hello" % "world";
        cout<< greetings <<endl;
    }
    catch(std::exception& e)
    {
        cout << "exception: " << e.what() << endl;
    }
    return 0;
}
```

### The second sample shows how to use the appender class:

```C++
#include <iostream>
#include <fmt11.h>

using namespace std;

int main()
{
    try {
        // fmt11::Append can concatenate values of different types into a string
        cout<< fmt11::Append("hello ") + "world " + "asdf " + 42 + ' ' + 3.14 <<endl;

        // operator << can be used too, but it has different precedence
        cout<< (fmt11::Append("hello ") << "world " << "asdf " << 42 << ' ' << 3.14) <<endl;

        // it also supports wide strings
        wcout<< fmt11::AppendW(L"hello ") + L"world " + L"asdf " + 42 + L' ' + 3.14 <<endl;
    }
    catch(std::exception& e)
    {
        cout << "exception: " << e.what() << endl;
    }
    return 0;
}
```

### The third sample shows how to use the library with custom types:

```C++
#include <iostream>
#include <fmt11.h>

using namespace std;

// a custom type, not supported by Append or Replace by default
struct CustomType {
    const char * str;
    CustomType(const char * str)
        : str(str) {}
};

// by overloading to_string (or to_wstring for the wide string versions) to accept the CustomType,
// it becomes usable in Append and Replace
inline const std::string to_string(const CustomType& ct) { return std::string(ct.str); }

int main()
{
    try {
        // custom types can be used by implementing to_string for it
        cout<< fmt11::Append("hello ") + CustomType("world") <<endl;
        cout<< fmt11::Replace("hello %s") % CustomType("world") <<endl;
    }
    catch(std::exception& e)
    {
        cout << "exception: " << e.what() << endl;
    }
    return 0;
}
```

### The goals of fmt11 are:

- to be elegantly written with good C++ design, STL, exceptions and RAII idiom
- to keep dependencies to a minimum (STL)
- to be portable
- to support wide strings
- to be light and fast
- to have a good unit test coverage
- to be well documented, and with some good examples
- to be well maintained
- to use a permissive MIT license, similar to BSD or Boost, for proprietary/commercial usage

### Supported platforms:

Developements and tests are done under the following compilers / OSs:
- G++ 4.8.4 on Ubuntu 14.04 x86_64
- Clang++ 3.4 on Ubuntu 14.04 x86_64
- Visual Studio 2013 compiler on Windows 7

### Dependencies

- C++11 support
- an STL implementation
- exception support (the class Exception inheritx from std::runtime_error)

### Installation

To use this library, just copy the single header file to your projects include path.

### Building the examples:

#### CMake and test
A CMake configuration file is also provided for multiplatform support and testing.

Building the example:

```bash
cd example
mkdir build
cd build
cmake ..
make
```

Building and executing the tests:

```bash
cd test
mkdir build
cd build
cmake ..
make
ctest --output-on-failure
```

### License

Copyright (c) 2015 Peter Belanyi (peter.belanyi@gmail.com)

Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt or copy at http://opensource.org/licenses/MIT)

## How to contribute
### GitLab website
The most efficient way to help and contribute to this library is to use the tools provided by GitLab:
- please fill bug reports and feature requests here: https://gitlab.com/almonsin/fmt11/issues
- fork the repository, make some small changes and submit them with pull-request

### Contact
You can also email me directly, I will answer any questions and requests.
