/**
* A simple and easy to use string formatting library for C++ 11
*
* Copyright (c) 2015 Peter Belanyi (peter.belanyi@gmail.com)
*
* Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
* or copy at http://opensource.org/licenses/MIT)
*/

#ifndef FMT11_LIBRARY_GUARD
#define FMT11_LIBRARY_GUARD


#include <string>
#include <stdexcept>

namespace fmt11 {


// utils for converting to string
using std::to_string;

inline const std::string& to_string(const std::string& str)
{
    return str;
}

inline const std::string to_string(const char * str)
{
    return std::string(str);
}

inline const std::string to_string(char chr)
{
    return std::string(1, chr);
}

struct ToString {
    typedef std::string StringType;

    template <class T>
    inline static const StringType toString(T t)
    {
        return to_string(t);
    }

    inline static const StringType placeholder()
    {
        return "%s";
    }
};


// utils for converting to wide string
using std::to_wstring;

inline const std::wstring& to_wstring(const std::wstring& str)
{
    return str;
}

inline const std::wstring to_wstring(const wchar_t * str)
{
    return std::wstring(str);
}

inline const std::wstring to_wstring(wchar_t chr)
{
    return std::wstring(1, chr);
}

struct ToStringW {
    typedef std::wstring StringType;

    template <class T> inline static const StringType toString(T t)
    {
        return to_wstring(t);
    }
    
    inline static const StringType placeholder()
    {
        return L"%s";
    }
};


// generic Appender class
template <class ToStringImpl>
class AppendGeneric {
    public:
        AppendGeneric()
            : buffer()
        {
        }

        // initialize from anything that ToStringImpl::toString can convert
        template <class T>
        AppendGeneric(T s)
            : buffer(ToStringImpl::toString(s))
        {
        }

        // convert the parameter to string and append to the buffer
        template <class T>
        AppendGeneric& operator+(T s)
        {
            buffer += ToStringImpl::toString(s);
            return *this;
        }

        // same as operator +, added for convenience
        template <class T>
        AppendGeneric& operator<<(T s)
        {
            return *this + s;
        }

        // return buffer as string
        const typename ToStringImpl::StringType& str() const
        {
            return buffer;
        }

        // cast to string
        operator const typename ToStringImpl::StringType&() const
        {
            return str();
        }
    protected:
        typename ToStringImpl::StringType buffer;
};


// Appenders specialised for std::string and std::wstring
typedef AppendGeneric<ToString> Append;
typedef AppendGeneric<ToStringW> AppendW;


// stream inserters for specialised Appenders
inline std::ostream& operator<<(std::ostream& os, const Append& appender)
{
    os << appender.str();
    return os;
}

inline std::wostream& operator<<(std::wostream& os, const AppendW& appender)
{
    os << appender.str();
    return os;
}


// exception class of the fmt11 library
class Exception : public std::runtime_error {
    public:
        explicit Exception(const std::string& aErrorMessage)
            : std::runtime_error(aErrorMessage)
        {
        }
};


// generic Replacer class
template <class ToStringImpl>
class ReplaceGeneric {
    public:
        // initialize with format string, default placeholder is "%s"
        ReplaceGeneric(const typename ToStringImpl::StringType& formatString)
            : buffer(formatString), placeholder(ToStringImpl::placeholder())
        {
        }

        // initialize with format string and placeholder
        ReplaceGeneric(const typename ToStringImpl::StringType& formatString, const typename ToStringImpl::StringType& placeholder)
            : buffer(formatString), placeholder(placeholder)
        {
        }

        // convert the parameter to string and replace the first occurence of the placeholder in buffer with it
        template <class T>
        ReplaceGeneric& operator%(T s)
        {
            size_t index = buffer.find(placeholder);
            if(index == ToStringImpl::StringType::npos)
                throw fmt11::Exception("Placeholder not found in format string.");
            buffer.replace(index, placeholder.size(), ToStringImpl::toString(s));
            return *this;
        }

        // same as operator %, added for convenience
        template <class T>
        ReplaceGeneric& operator<<(T s)
        {
            return *this % s;
        }

        // return buffer as string
        const typename ToStringImpl::StringType& str() const
        {
            return buffer;
        }

        // cast to string
        operator const typename ToStringImpl::StringType&() const
        {
            return str();
        }
    protected:
        typename ToStringImpl::StringType buffer, placeholder;
};


// Replacers specialised for std::string and std::wstring
typedef ReplaceGeneric<ToString> Replace;
typedef ReplaceGeneric<ToStringW> ReplaceW;


// stream inserters for specialised Replacers
inline std::ostream& operator<<(std::ostream& os, const Replace& replacer)
{
    os << replacer.str();
    return os;
}

inline std::wostream& operator<<(std::wostream& os, const ReplaceW& replacer)
{
    os << replacer.str();
    return os;
}


} // namespace fmt11


#endif
