/**
* Copyright (c) 2015 Peter Belanyi (peter.belanyi@gmail.com)
*
* Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
* or copy at http://opensource.org/licenses/MIT)
*/

#include <iostream>

#include <fmt11.h>

using namespace std;

// a custom type, not supported by Append or Replace by default
struct CustomType {
    const char * str;
    CustomType(const char * str)
        : str(str) {}
};

// by overloading to_string to accept the CustomType it becomes usable in Append and Replace
inline const std::string to_string(const CustomType& ct) { return std::string(ct.str); }

int main()
{
    try {
        // fmt11::Append can concatenate values of different types into a string
        cout<< fmt11::Append("hello ") + "world " + "asdf " + 42 + ' ' + 3.14 <<endl;
    
        // operator << can be used too, but it has different precedence
        cout<< (fmt11::Append("hello ") << "world " << "asdf " << 42 << ' ' << 3.14) <<endl;
    
        // it also supports wide strings
        wcout<< fmt11::AppendW(L"hello ") + L"world " + L"asdf " + 42 + L' ' + 3.14 <<endl;
    
    
        // fmt11::Replace takes a format string a replaces the placeholders with the provided values (of different types)
        cout<< fmt11::Replace("%s %s, %s %s!") % "hello" % "world" % 42 % 3.14 <<endl;
    
        // operator << can be used too, but it has different precedence
        cout<< (fmt11::Replace("%s %s, %s %s!") << "hello" << "world" << 42 << 3.14) <<endl;
    
        // it also supports wide strings
        wcout<< fmt11::ReplaceW(L"%s %s, %s %s!") % L"hello" % L"world" % 42 % 3.14 <<endl;
    
        // the placeholder can be freely chosen
        cout<< fmt11::Replace("{} {}, {} {}!", "{}") % "hello" % "world" % 42 % 3.14 <<endl;

        // besides piping into streams, Append and Replace can be converted to std::string implicitly,
        // or explicitly by calling the .str() method
        string greetings = fmt11::Replace("{} {}", "{}") % "hello" % "world";
        cout<< greetings <<endl;

        // custom types can be used by implementing to_string for it
        cout<< fmt11::Append("hello ") + CustomType("world") <<endl;
        cout<< fmt11::Replace("hello %s") % CustomType("world") <<endl;
    }
    catch(std::exception& e)
    {
        cout << "exception: " << e.what() << endl;
    }
}
