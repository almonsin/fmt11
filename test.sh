#!/bin/bash

if [[ $1 == "example" ]]
then
	cd example
	./build.sh
elif [[ $1 == "test" ]]
then
	cd test
	./runtest.sh
else
	echo "invalid argument, expected 'test' or 'example'"
fi
